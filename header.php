<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="images/banner2.jpg"/>
    <title>DYFI KANNUR</title>
    <link rel="icon" href="images/fav_ico.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/jqueryFlexisel.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="light/lightgallery.css">
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.flexisel.js"></script>
    <script src="js/script.js"></script>
    
    
</head>
    
<body>
    <!-- Header -->
    <div class="header">
        <div class="container">
            <div class="logo">
                <a href="index.html">
                    <img src="images/logo.png" alt="DYFI" />
                </a>
                <div class="logo_desc">
                    <h3>Democratic<br />Youth Federation of India</h3><br />
                    <h4>Kannur District Committee</h4>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    <div class="nav_wrap">        
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <span class="nav_trigger"><i class="fa fa-bars"></i></span>
                    <ul class="navigation">
                        <li><a href="index.php">home</a></li>
                        <li><a href="aboutus.html">about us</a></li>
                        <li><a href="committee.php">committee</a></li>
                        <li><a href="martyrs.html">Martyrs</a></li>
                        <li><a href="gallery.php">gallery</a></li>
                        <li><a href="contact.php">contact us</a></li>
                    </ul>
                    <div class="soc">
                        <span><img src="images/hand.png" alt="" /></span>
                        <a href="https://www.facebook.com/dyfi.kannurdc.7" target="_blank"><i class="fa fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Header -->