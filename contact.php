<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DYFI KANNUR</title>
    <link rel="icon" href="images/fav_ico.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    
    
</head>
    
<body>
    <!-- Header -->
    <div class="header">
        <div class="container">
            <div class="logo">
                <a href="index.php">
                    <img src="images/logo.png" alt="DYFI" />
                </a>
                <div class="logo_desc">
                    <h3>Democratic<br />Youth Federation of India</h3><br />
                    <h4>Kannur District Committee</h4>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    <div class="nav_wrap">        
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <span class="nav_trigger"><i class="fa fa-bars"></i></span>
                    <ul class="navigation">
                        <li><a href="index.php">home</a></li>
                        <li><a href="aboutus.html">about us</a></li>
                        <li><a href="committee.php">committee</a></li>
                        <!--<li><a href="news.html">news &amp; events</a></li>-->
                        <li><a href="martyrs.html">Martyrs</a></li>
                        <li><a href="gallery.php">gallery</a></li>
                        <li><a href="#">contact us</a></li>
                    </ul>
                    <div class="soc">
                        <span><img src="images/hand.png" alt="" /></span>
                        <a href="https://www.facebook.com/dyfi.kannurdc.7" target="_blank"><i class="fa fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Header -->
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page_head page_head_large"><h3>contact us</h3></div>
            </div>
        </div> 
        <!--<div class="row">
            <div class="col-lg-12">
                <p class="text-center">
                    ഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനംഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനംഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനംഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനംഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനംഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനംഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനംഒരു വ്യക്തിയുടെ ശാരീരിക മാനസിക ഭൗതിക ആധ്യാത്മിക സന്തുലനം
                </p>
            </div>
        </div>-->
        <div class="row" style="margin-top: 30px;">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="contact_desc">
                    <div class="contact_ico">
                        <img src="images/welcome_img.png" alt="" />
                    </div>
                    <p>Youth Centre,<br />DYFI District Committee Office, <br />Kannur-2</p>
                    <p><span><i class="fa fa-phone"></i></span> &nbsp;0497-2712859</p>
                    <p><span><i class="fa fa-envelope"></i></span> &nbsp;<a href="mailto:info@dyfikannur.com">info@dyfikannur.com</a></p>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="contact_form">
                    <form method="post" action="mail.php">
                        <div class="form_block">
                            <label>Name</label>
                            <input type="text" name="name" placeholder="Please enter your name" required />
                        </div>
                        <div class="form_block">
                            <label>Email</label>
                            <input type="email" name="name" placeholder="Enter you email ID" required />
                        </div>
                        <div class="form_block">
                            <label>Contact Number</label>
                            <input type="text" name="mobile" placeholder="Please enter your contact number" required />
                        </div>
                        <div class="form_block">
                            <label>Subject</label>
                            <input type="text" name="subject" placeholder="Subject" required />
                        </div>
                        <div class="form_block">
                            <label>Message</label>
                            <textarea name="message" placeholder="Enter your message" required></textarea>
                        </div>
                        <div class="form_block">
                            <input type="submit" name="submit" value="Send" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-5">
                    <div class="footer_logo">
                        <a href="index.php"><img src="images/logo.png" alt="DYFI" /></a>
                        <div class="footer_logo_desc">
                            <h3>Democratic<br />Youth Federation of India</h3>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-3">
                    <div class="footer_soc">
                        <a href="https://www.facebook.com/dyfi.kannurdc.7" target="_blank"><i class="fa fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa fa-youtube"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="copy">
                        <p>All Rights Reserved &copy; DYFI Kannur</p>
                        <p>Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi_logo.png" alt="Bodhiinfo" /></a></p>
                    </div>
                </div> 
            </div>                       
        </div>
    </div>
</body>
</html>
