<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
//header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':			
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$galleryImage = $_FILES['galleryImage']['tmp_name'];
			$result = date("YmdHis");//$date->format('YmdHis');
			$path_info = pathinfo($_FILES['galleryImage']['name']);
			$upload_file_name = $result.'_0.'.$path_info['extension'];
			copy($galleryImage,"../../images/uploaded/$upload_file_name");
				
				
			$data['galleryPath'] = $App->convert("images/uploaded/".$upload_file_name);	
			$data['title'] = $App->convert($_REQUEST['title']);
			$data['description'] = $App->convert($_REQUEST['description']);
			$db->query_insert(TABLE_GALLERY,$data);						
			$db->close();				
			$_SESSION['msg']="<font color='green'>Image Added to Gallery Successfully</font>";					
			header("location:new.php");	
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  			
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				if(!empty($_FILES['galleryImage']['name']))
				{
					$selectGalImg = "select * from ".TABLE_GALLERY." where ID=".$editId;
					$resSelectGalImg = mysql_query($selectGalImg);
					$rowSelectGalImg = mysql_fetch_array($resSelectGalImg);
					$curGalImgPath = "../../".$rowSelectGalImg['galleryPath'];
					unlink($curGalImgPath);		
					
					
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$galleryImage = $_FILES['galleryImage']['tmp_name'];
					$result = date("YmdHis");//$date->format('YmdHis');
					$path_info = pathinfo($_FILES['galleryImage']['name']);
					$upload_file_name = $result.'_0.'.$path_info['extension'];
					copy($galleryImage,"../../images/uploaded/$upload_file_name");
						
						
					$data['galleryPath'] = $App->convert("images/uploaded/".$upload_file_name);	
				}
				
				$data['title']	 =	$App->convert($_REQUEST['title']);
				$data['description']	 =	$App->convert($_REQUEST['description']);
					
				$db->query_update(TABLE_GALLERY,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="<font color='green'>Gallery Details Updated successfully</font>";					
				header("location:new.php");		
		
		break;		
	// DELETE SECTION
		case 'delete':
			$id = $_REQUEST['id'];
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			
			$selectGalImg = "select * from ".TABLE_GALLERY." where ID=".$id;
			$resSelectGalImg = mysql_query($selectGalImg);
			$rowSelectGalImg = mysql_fetch_array($resSelectGalImg);
			$curGalImgPath = "../../".$rowSelectGalImg['galleryPath'];
			unlink($curGalImgPath);	
			
			$db->query("DELETE FROM `".TABLE_GALLERY."` WHERE ID='{$id}'");								
			$db->close();
			$_SESSION['msg']="<font color='green'>Image Deleted Successfully</font>";					
			header("location:new.php");	
						
		break;
}
?>


















