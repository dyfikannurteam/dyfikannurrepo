<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}



</script>

<?php
 if(isset($_SESSION['msg'])){?><?php echo $_SESSION['msg']; ?><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">GALLERY</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4" >
          </div>
        </div>
		<div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive committee_table">
              <table class="table table_admin view_limitter pagination_table" >
                <thead>
                  <tr>
                    <th>Sl No</th>
					<th>Image</th>								
					<th>Title</th>								
					<th>Description</th>								
                  </tr>
                </thead>
                <tbody>
						<?php 
						$i=1;
						$select1 = mysql_query("select * from ".TABLE_GALLERY." order by ID desc");
						$number=mysql_num_rows($select1);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="4">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
							while($row=mysql_fetch_array($select1))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
						<td><?php echo $i; $i++;?>
						  <div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  </div></td>
						
						<td><div class="banr_img"><img src="../../<?= $row['galleryPath']; ?>" /></div></td>	
						<td><?= $row['title']; ?></td>	
						<td><?= $row['description']; ?></td>	
					  </tr>
					  <?php }
					  }
					  ?>                  
                </tbody>
              </table>              
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-12 page_numbers text-center">
                <div class="btn-group pager_selector">
                </div>
            </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">ADD NEW IMAGE TO GALLERY</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-12">                
                    <div class="form-group">
                      <label for="countType">Upload Image: * (900x400) </label>
                      <input id="image1" name="galleryImage" type="file" required="required" class="file-loading" accept="image/*">
                    </div>               
                    <div class="form-group">
                      <label for="countType">Title: </label>
                      <input type="text" class="form-control2" name="title" id="name" >
                    </div>              
                    <div class="form-group">
                      <label for="countType">Description: </label>
                      <textarea name="description" class="form-control2" style="min-height: 100px;"></textarea>
                    </div>     
					</div>                 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
