<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
//header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="<font color='red'>Error, Invalid Details!</font>";				
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$data['event']	  		=	$App->convert($_REQUEST['name']);						
				$data['description']	  		=	$App->convert($_REQUEST['description']);	
				$data['eventDate']	  		=	$App->dbformat_date($_REQUEST['date']);						
					
				$db->query_insert(TABLE_EVENTS,$data);							
				$db->close();				
				$_SESSION['msg']="<font color='green'>Category Added Successfully</font>";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['name'])
			{	
				$_SESSION['msg']="<font color='red'>Error, Invalid Details.</font>";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['event']	 =	$App->convert($_REQUEST['name']);
				$data['description']	 =	$App->convert($_REQUEST['description']);
				$data['eventDate']	 =	$App->dbformat_date($_REQUEST['date']);
				$db->query_update(TABLE_EVENTS,$data," ID='{$editId}'");							
				$db->close();
				
				$_SESSION['msg']="<font color='green'>Event Details Updated successfully</font>";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
		case 'delete':
			$id = $_REQUEST['id'];
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			
			$db->query("DELETE FROM `".TABLE_EVENTS."` WHERE ID='{$id}'");								
			$db->close();
			$_SESSION['msg']="<font color='green'>Event Deleted Successfully</font>";					
			header("location:new.php");	
						
		break;
}
?>


















