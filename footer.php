 <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-5">
                    <div class="footer_logo">
                        <a href="index.html"><img src="images/logo.png" alt="DYFI" /></a>
                        <div class="footer_logo_desc">
                            <h3>Democratic<br />Youth Federation of India</h3>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-3">
                    <div class="footer_soc">
                        <a href="https://www.facebook.com/dyfi.kannurdc.7" target="_blank"><i class="fa fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa fa-youtube"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="copy">
                        <p>All Rights Reserved &copy; DYFI Kannur</p>
                        <p>Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi_logo.png" alt="Bodhiinfo" /></a></p>
                    </div>
                </div> 
            </div>                       
        </div>
    </div>
    
    
    
    
    <script>
        $(function(){
            // Banner
            $('.banner').bxSlider({
                pager: false,
                auto: true
            });
            // News & Events in Home Page
            $('.hm_news_slider').bxSlider({
                auto: true,
                pager: false,
                nextSelector: '#hm_news_next',
                prevSelector: '#hm_news_prev',
                nextText: '<i class="fa fa-caret-right"></i>',
                prevText: '<i class="fa fa-caret-left"></i>',
                adaptiveHeight: true
            });    
        });
        
        $(window).load(function(){
            $("#flexiselDemo3").flexisel({
                visibleItems: 3,
                animationSpeed: 500,
                autoPlay: true,
                autoPlaySpeed: 3000,            
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: { 
                    portrait: { 
                        changePoint:480,
                        visibleItems: 1
                    }, 
                    landscape: { 
                        changePoint:640,
                        visibleItems: 2
                    },
                    tablet: { 
                        changePoint:768,
                        visibleItems: 3
                    }
                }
            });
            $("#flexiselDemo2").flexisel({
                visibleItems: 3,
                animationSpeed: 500,
                autoPlay: true,
                autoPlaySpeed: 3000,            
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: { 
                    portrait: { 
                        changePoint:480,
                        visibleItems: 1
                    }, 
                    landscape: { 
                        changePoint:640,
                        visibleItems: 2
                    },
                    tablet: { 
                        changePoint:768,
                        visibleItems: 3
                    }
                }
            });
        });
        
    </script>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
</body>
</html>
