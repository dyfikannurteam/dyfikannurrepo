<?php
	require('config.inc.php');
	include('header.php');
?>

<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page_head"><h3>gallery</h3></div>
            </div>
        </div> 
        <div class="row">
            <div class="col-lg-12">
                <div class="gal_wrap">
                    <div id="lightgallery">
                    	<?php
                    	$galSelect = "select * from ".TABLE_GALLERY." order by ID desc";
                    	$galQry = mysqli_query($connection, $galSelect);
                    	if(mysqli_num_rows($galQry) > 0){
							while($galRow = mysqli_fetch_array($galQry))
							{
							?>
							
							<a href="webadmin/<?= $galRow['galleryPath']; ?>" data-sub-html="<h4><?= $galRow['title']; ?></h4><?php if($galRow['description'] != ''){ ?><p><?= $galRow['description']; ?></p><?php } ?>">
	                            <div class="pop_inner">
	                                <img src="webadmin/<?= $galRow['galleryPath']; ?>" alt="">
	                                <div class="aover"></div>
	                            </div>                
	                        </a>
							<?php
							}
						}
                    	?>                                       
                    </div>
                    <div class="bd_clear"></div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <!-- Light box -->
    <script src="light/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="light/lg-fullscreen.min.js"></script>
    <script src="light/lg-thumbnail.min.js"></script>
    <script>
        $(function(){
            $("#lightgallery").lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false
            });      
        });
    </script>
    
<?php  include('footer.php'); ?>