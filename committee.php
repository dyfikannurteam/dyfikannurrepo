<?php
require('config.inc.php');
include('header.php');
?>

<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page_head"><h3>committe</h3></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="committe_tbl table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>പേര്</th>
                                <th>സ്ഥാനം</th>
                                <th>ഫോണ്‍ നമ്പർ</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $comSelect = "select * from ".TABLE_COMMITTEE. " order by ID asc";
                        $comQry = mysqli_query($connection, $comSelect);
                        if(mysqli_num_rows($comQry) > 0){
							while($comRow = mysqli_fetch_array($comQry)){
							?>
							<tr>
								<td><?= $comRow['name']; ?></td>
								<td><?= $comRow['designation']; ?></td>
								<td><?= $comRow['contact']; ?></td>
							</tr>
							<?php
							}
						}
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
<?php
include('footer.php');
?>