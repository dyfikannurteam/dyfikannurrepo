/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function(){
    'use strict';
    
    // Navigation
    $('.nav_trigger').click(function(){
        $('.navigation').slideToggle();    
    });
    $(window).resize(function(){
        var windWidth = $(window).width();
        if(windWidth >= 768)
        {
            $('.navigation').show();
        }
        else
        {
            $('.navigation').hide();
        }
    });
});
